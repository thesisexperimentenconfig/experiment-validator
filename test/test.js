var validator = require('../index.js');

var should = require('chai').should();
var expect = require('chai').expect;

describe('Test validator', function(){
	
	it('test isValidConnection()', function(){
		var fromModel = {};
		
		var result = validator.isValidConnection(fromModel, fromModel);
		result.should.eql(false);
		
		fromModel  = ["output1"];
		var toModel =  ["output12","output2"];

		result = validator.isValidConnection(fromModel, toModel);
		result.should.eql(false);
		
		toModel = ["output1","output2"]
		
		result = validator.isValidConnection(fromModel, toModel);
		result.should.eql(true);
		
	});
	
	it('test isValidModel() valid scenarios', function(){
		var model = {
			"parameters": {
	            "protease": {
	                "name": "Protease",
	                "type": "enum",
					"value": "trypsin",
	                "enum": [
	                    "trypsin"
	                ]
	            },
	            "duration": {
	                "name": "Duration",
	                "type": "number",
	                "min": 30,
	                "max": 1440,
	                "value": 30,
	                "unit": "min."
	            },
	            "temperature": {
	                "name": "Temperature",
	                "type": "number",
	                "min": 1,
	                "max": 1000,
	                "value": 37,
	                "unit": "&#176;C"
	            }
	        }
		};
		
		var result = validator.isValidParameter(model.parameters.temperature);
		result.should.eql(true);
		
		result = validator.isValidParameter(model.parameters.duration);
		result.should.eql(true);
		
		result = validator.isValidParameter(model.parameters.protease);
		result.should.eql(true);
		
		var result = validator.isValidModel(model);
		result.should.eql(true);
		
		var result = validator.isValidModel(model);
		result.should.eql(true);
		
		
		
		result = validator.isValidModel({});
		result.should.eql(true);
		
		delete model.parameters.protease.value;
		result = validator.isValidParameter(model.parameters.protease);
		result.should.eql(true);
		
		delete model.parameters.duration.value;
		result = validator.isValidParameter(model.parameters.duration);
		result.should.eql(true);
		
		
	});
	
	it('test isValidModel() invalid scenarios', function(){
		
		var model = {
			"parameters": {
	            "protease": {
	                "name": "Protease",
	                "type": "otherType",
	                "enum": [
	                    "trypsin"
	                ],
					"required": true
	            },
	            "duration": {
	                "name": "Duration",
	                "type": "number",
	                "max": 1440,
	                "unit": "min.",
					"required": true
	            },
	            "temperature": {
	                "name": "Temperature",
	                "type": "number",
	                "min": 1,
	                "max": 1000,
	                "value": 37,
	                "unit": "&#176;C"
	            }
	        }
		};
		
		var result = validator.isValidParameter(model.parameters.duration);
		result.should.eql(false);
		
		result = validator.isValidNumberParameter({
			type: "number",
			value: "d",
			min: 1,
			max: 5
		});
		result.should.eql(false);
		
		result = validator.isValidModel(model);
		result.should.eql(false);
		
		model.parameters.temperature.value = 2000;
		result = validator.isValidModel(model);
		result.should.eql(false);
		
		result = validator.isValidModel();
		result.should.eql(false);
		
		result = validator.isValidParameter();
		result.should.eql(false)
		
		result = validator.isValidEnumParameter();
		result.should.eql(false)
		
		result = validator.isValidEnumParameter(model.parameters.protease);
		result.should.eql(false);
		
		result = validator.isValidNumberParameter();
		result.should.eql(false);
		
		model.parameters.temperature.value = -1;
		result = validator.isValidNumberParameter(model.parameters.temperature);
		result.should.eql(false);
		
		model.parameters.temperature.value = 4000;
		result = validator.isValidNumberParameter(model.parameters.temperature);
		result.should.eql(false);
		
		
	});
	
	it('test #isAcyclicGraph() no cycle', function(){
		var connections = [
			{
				from: {
					id: 'a'
				},
				to: {
					id: 'b'
				}
			},
			{
				from: {
					id: 'b'
				},
				to: {
					id: 'c'
				}
			},
			{
				from: {
					id: 'b'
				},
				to: {
					id: 'd'
				}
			},
			{
				from: {
					id: 'c'
				},
				to: {
					id: 'e'
				}
			}
		]
		var result = validator.isAcyclicGraph(connections);
		result.should.eql(true);
	});
	
	it('test #isAcyclicGraph() cycle', function(){
		var connections = [
			{
				from: {
					id: 'a'
				},
				to: {
					id: 'b'
				}
			},
			{
				from: {
					id: 'b'
				},
				to: {
					id: 'c'
				}
			},
			{
				from: {
					id: 'b'
				},
				to: {
					id: 'd'
				}
			},
			{
				from: {
					id: 'c'
				},
				to: {
					id: 'a'
				}
			}
		];
		var result = validator.isAcyclicGraph(connections);
		result.should.eql(false);
		
		connections = [
			{
				from: {
					id: 'a'
				},
				to: {
					id: 'b'
				}
			},
			{
				from: {
					id: 'b'
				},
				to: {
					id: 'a'
				}
			},
		];
		
		result = validator.isAcyclicGraph(connections);
		result.should.eql(false);
	});
})
