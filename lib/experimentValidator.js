var _ = require('lodash');

module.exports = {
    
    /**
     * Checks whether a connection between two models is valid.
     * Does this by  matching the output parameter to the input
     * parameter.
     */
    isValidConnection: function(output, input){
        if(!output || !input){
            return false;
        }
        
        if(_.contains(output, '*') || _.contains(input, '*')){
            return true
        }
        
        var intersection = _.intersection(output, input);
        if(intersection.length > 0){
            return true;
        }
        
        return false;
    },
    
    /**
     * Checks whether a model is valid. Does this by checking
     * all the parameters
     */
    isValidModel: function(model){
        
        // if null or undefined, the model is not valid
        if(!model){
            return false;
        }
        //if the model does not have parameters, it is automatically valid.
        if(!model.parameters){
            return true;
        }
        
        var parameters = model.parameters;
        
        // check all parameters
        for(key in parameters){
            if(!this.isValidParameter(parameters[key])){
                return false;
            }
        }
        
        return true;
        
    },
    
     /**
     * Checks whether a parameter is valid
     */
    isValidParameter: function(parameter){
        if(!parameter){
            return false;
        }
        
        if(parameter.type.toLowerCase() === 'enum'){
            return this.isValidEnumParameter(parameter);
        }
        
        if(parameter.type.toLowerCase() === 'number'){
            return this.isValidNumberParameter(parameter);
        }
        
        return true;
    },
    
    /**
     * Checks whether an enumeration parameter is valid
     */
    isValidEnumParameter: function(enumParameter){
        
        if(!enumParameter){
            return false;
        }
        
        
        
        if(!enumParameter.value && !enumParameter.required){
            return true;
        }
        
        if(!enumParameter.value){
            return false;
        }
        
        return _.contains(enumParameter.enum, enumParameter.value);
    },
    
     /**
     * Checks whether a number parameter is valid
     */
    isValidNumberParameter: function(numberParameter){
        
        if(!!numberParameter && !!numberParameter.value && isNaN(numberParameter.value)){
            return false;
        }
        
        if(!numberParameter){
            return false;
        }
        
        if(!numberParameter.value && !numberParameter.required){
            return true;
        }
        
        if(!numberParameter.value){
            return false;
        }
        
        
        if(parseFloat(numberParameter.value) > parseFloat(numberParameter.max)){
            return false;
        }
        
        if(parseFloat(numberParameter.value) < parseFloat(numberParameter.min))
            return false;
            
            
        return true;
    },
        
    
    /**
     * Checks whether a graph is acyclical.
     * @param [object] expects an array of objects with to and from fields.
     */
    isAcyclicGraph: function(connections){
        // bepaal voor elke node zijn kinderen
        for(var i = 0; i < connections.length; i++){
            var connection = connections[i];
            var fromNode = connection.from;
            if(this.isOwnChild(fromNode, fromNode, connections)) {
                return false;
            }
        }
        
        return true;
    },
    
    isOwnChild: function(node, startNode, connections){
        var children = [];

        for(var j = 0; j < connections.length; j++){
            var connection = connections[j];
            var fromNode = connection.from;
            var toNode = connection.to;
            
            if(_.isEqual(toNode, startNode) && _.isEqual(fromNode, node)){
                return true;
            }
            if(_.isEqual(fromNode, node)){
                children.push(toNode);
            }
         
        }
         
         if(children.length === 0){
             return false;
         }
         
         for(var i = 0; i < children.length; i++){
             var n = children[i];
             if(this.isOwnChild(n, startNode, connections)){
                 return true;
             }
         }
         
         return false;
    }
}